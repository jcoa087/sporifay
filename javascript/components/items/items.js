




const items = document.querySelectorAll('.items__item')

items.forEach(element => {
    const 
    play = element.querySelector('.item__icon-play'),
    pause = element.querySelector('.item__icon-pause')
    play.addEventListener('click',()=>{
        play.classList.add('item__icon-play--active')
        pause.classList.add('item__icon-pause--active')
    })
});


items.forEach(element => {
    const 
    play = element.querySelector('.item__icon-play'),
    pause = element.querySelector('.item__icon-pause')
    pause.addEventListener('click',()=>{
        
        play.classList.remove('item__icon-play--active')
        pause.classList.remove('item__icon-pause--active')
    })
});


